/*
 * Author: Thiago Becker - beckerthiago@gmail.com
 * EntityBase.java
 */

package com.tbeckr.business.entity;

/**
 * Interface EntityBase deve ser implementada por todas as entidades
 * para o funcionamento do BaseDAO
 * @author tbecker
 *
 */
public interface EntityBase {	
	public Long getId();
}
