/*
 * Author: Thiago Becker - beckerthiago@gmail.com
 * Usuario.java
 */

package com.tbeckr.business.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

/**
 * Usuario - Representa uma entidade usuário no banco de dados
 * @author tbecker
 *
 */
@Entity // Anotação que diz pro JPA que esta classe é uma entidade
public class Usuario implements EntityBase {

	@Id // Define que este atributo será chave primária
	// Define o sequence do banco de dados para geração da chave primária
	@SequenceGenerator(name="identifier", sequenceName="usuario_id_seq", allocationSize=1)
	// Define que o valor será gerado automaticamente pelo sequence
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="identifier")
	private Long id;
	
	// Anotação que diz ao JPA que esta classe é uma coluna do banco de dados
	@Column
	private String nome;
	
	@Column
	private String login;
	
	@Column
	private String senha;

	/**
	 * Constructor padrão
	 */
	public Usuario() {
		
	}
	
	/**
	 * Constructor com os parâmetros
	 * @param id
	 * @param nome
	 * @param login
	 * @param senha
	 */
	public Usuario(Long id, String nome, String login, String senha) {
		super();
		this.id = id;
		this.nome = nome;
		this.login = login;
		this.senha = senha;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public String toString() {
		return this.nome;
	}
	
	
}
