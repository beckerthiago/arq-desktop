/*
 * Author: Thiago Becker - beckerthiago@gmail.com
 * UsuarioDAO.java
 */

package com.tbeckr.business.dal;

import java.util.List;

import com.tbeckr.business.entity.Usuario;

/**
 * UsuarioDAO - Classe de acesso a dados para a entidade Usuário
 * @author tbecker
 *
 */
public class UsuarioDAO extends BaseDAO<Usuario> {

	/**
	 * Retorna todos os usuários do banco de dados
	 * @return Lista com todos os usuários
	 */
	public List<Usuario> findAll() {
		return super.findAll(Usuario.class);
	}
}
