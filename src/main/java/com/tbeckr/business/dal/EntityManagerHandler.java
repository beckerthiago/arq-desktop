/*
 * Author: Thiago Becker - beckerthiago@gmail.com
 * EntityManagerHandler.java
 */

package com.tbeckr.business.dal;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 * Singleton que controla a instância do EntityManager
 * 
 * @author tbecker
 */
public class EntityManagerHandler {
	// Variável estática que guarda a instancia do EntityManager
	static private EntityManager instance;

	/** 
	 * @return a instância do EntityManager
	 */
	public static EntityManager getInstance() {
		if (instance == null) {
			instance = Persistence.createEntityManagerFactory("exemplo").createEntityManager();
		}
		
		return instance;
	}
}
