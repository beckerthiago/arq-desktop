/*
 * Author: Thiago Becker - beckerthiago@gmail.com
 * BaseDAO.java
 */

package com.tbeckr.business.dal;

import java.util.List;
import javax.persistence.EntityManager;
import com.tbeckr.business.entity.EntityBase;

/**
 * Esta classe é a base para todas as classes de acesso a banco de dados,
 * ela contém as operações básicas de CRUD (Create, Retrieve, Update, Delete).
 * 
 * @author tbecker
 *
 * @param <T> Tipo da Entidade a qual o DAO fará acesso
 */
public class BaseDAO<T extends EntityBase> {

	// Define a variável em com a instância do EntityManager
    private EntityManager em = EntityManagerHandler.getInstance();
	
    /**
     * Salva uma entidade no banco de dados
     * @param entity a entidade a ser salva
     * @return a entidade persistida
     */
	public T save(T entity) {
		try {			
			em.getTransaction().begin();
			em.persist(entity);		
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return entity;
	}
	
	/**
	 * Atualiza uma entidade no banco de dados
	 * @param entity a entidade a ser atualizada
	 * @return a entidade persistida
	 */
	public T update(T entity) {
		try {			
			em.getTransaction().begin();			
			em.merge(entity);
			em.getTransaction().commit();			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		
		return entity;
	}	
	
	/**
	 * Remove uma entidade no banco de dados
	 * @param entity a entidade a ser removida
	 */
	public void delete(T entity) {
		try {			
			em.getTransaction().begin();			
			em.remove(em.merge(entity));
			em.getTransaction().commit();			
		} catch (Exception e) {
			e.printStackTrace();			
		}
	}
	
	/**
	 * Busca uma entidade pela sua chave primária
	 * @param entity a classe da entidade a ser buscada
	 * @param id a chave primária da entidade a ser buscada
	 * @return a entidade retornada do banco de dados
	 */
	public T findById(Class<T> entity, Long id) {
		return em.find(entity, id); 
	}
	
	/**
	 * Busca todas as entidades de determinada classe
	 * @param entity a classe das entidades a serem buscadas
	 * @return
	 */
	public List<T> findAll(Class<T> entity) {
		return em.createQuery("select t from " + entity.getSimpleName() + " t").getResultList();
	}
	
	/** 
	 * @return a instância do entityManager
	 */
	public EntityManager getEntityManager() {
		return this.em;
	}
}
