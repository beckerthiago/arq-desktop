/*
 * Author: Thiago Becker - beckerthiago@gmail.com
 * Usuario.java
 */

package com.tbeckr.config;

import java.awt.EventQueue;

import javax.persistence.EntityManager;
import javax.swing.UIManager;

import com.tbeckr.business.dal.EntityManagerHandler;
import com.tbeckr.gui.MainWindow;

/**
 * Classe MAIN
 * @author tbecker
 *
 */
public class Main {

	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					/* Acessa a instância do EntityManager para inicializar o 
					 *  acesso ao banco de dados (feito no inicio pois a operação leva alguns segundos
					 *  para ser executada, se deixar para o primeiro acesso real ao banco, acaba causando
					 *  uma demora para carregar os dados na primeira vez)
					 */
					EntityManager em = EntityManagerHandler.getInstance();
					
					// Instancia a janela principal da aplicação
					MainWindow window = new MainWindow();		
					
					

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
