/*
 * Author: Thiago Becker - beckerthiago@gmail.com
 * UsuariosFrame.java
 */

package com.tbeckr.gui.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.tbeckr.gui.controller.UsuarioController;

import java.awt.FlowLayout;

public class UsuariosFrame extends JFrame {

	private JPanel contentPane;
	private UsuarioController usuarioController;

	/**
	 * Create the frame.
	 */
	public UsuariosFrame(UsuarioController controller) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 801, 646);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		this.usuarioController = controller;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	public JPanel getContentPane() {
		return this.contentPane;
	}
	
	public UsuarioController getUsuarioController() {
		return usuarioController;
	}

	public void setUsuarioController(UsuarioController usuarioController) {
		this.usuarioController = usuarioController;
	}

}
