/*
 * Author: Thiago Becker - beckerthiago@gmail.com
 * ListUsuarioPanel.java
 */

package com.tbeckr.gui.view;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.BorderLayout;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.tbeckr.gui.controller.UsuarioController;

import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;

public class ListUsuarioPanel extends JPanel {
	private JTable table;
	
	// O controller que controla esta interface
	private UsuarioController usuarioController;

	/**
	 * Cria o painel
	 * o controller responsável por esta interface deve estar no contructor
	 */
	@SuppressWarnings("serial")
	public ListUsuarioPanel(UsuarioController controller) {
		setLayout(new BorderLayout(0, 0));
		
		// seta a variável do controler.
		this.usuarioController = controller;
		
		JPanel panelCenter = new JPanel();
		add(panelCenter, BorderLayout.CENTER);
		
		JScrollPane scrollPane = new JScrollPane();
		panelCenter.add(scrollPane);
		
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Id", "Nome", "Login"
			}
		) {
			Class[] columnTypes = new Class[] {
				Integer.class, Object.class, Object.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				controller.updateUsuarioSelecionado();
			}
						
		});
		
		scrollPane.setViewportView(table);
		
		JPanel panelBottom = new JPanel();
		add(panelBottom, BorderLayout.SOUTH);
		
		JButton btnNovo = new JButton("Novo");
		btnNovo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// chama o metodo do controller que gerencia o evento
				usuarioController.novoUsuario();
			}
		});
		panelBottom.add(btnNovo);
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// chama o metodo do controller que gerencia o evento
				usuarioController.fecharList();
			}
		});
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// chama o metodo do controller que gerencia o evento
				usuarioController.editar();
			}
		});
		panelBottom.add(btnEditar);
		
		JButton btnRemover = new JButton("Remover");
		btnRemover.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// chama o metodo do controller que gerencia o evento
				usuarioController.remover();
			}
		});
		panelBottom.add(btnRemover);
		panelBottom.add(btnFechar);
	}
	
	public JTable getTable() {
		return this.table;
	}
	
	public UsuarioController getUsuarioController() {
		return usuarioController;
	}

	public void setUsuarioController(UsuarioController usuarioController) {
		this.usuarioController = usuarioController;
	}

}
