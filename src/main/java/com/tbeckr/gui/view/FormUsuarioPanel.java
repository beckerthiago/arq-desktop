/*
 * Author: Thiago Becker - beckerthiago@gmail.com
 * FormUsuarioPanel.java
 */

package com.tbeckr.gui.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.tbeckr.gui.controller.UsuarioController;
import java.awt.GridLayout;
import javax.swing.SwingConstants;
import net.miginfocom.swing.MigLayout;

public class FormUsuarioPanel extends JPanel {
	private JTextField tfNome;
	private JTextField tfUsuario;
	private JTextField tdSenha;
	
	// O controller que controla esta interface
	private UsuarioController usuarioController;

	/**
	 * Cria o painel
	 * o controller responsável por esta interface deve estar no contructor
	 */
	public FormUsuarioPanel(UsuarioController controller) {
		setLayout(new BorderLayout(0, 0));
		
		// seta a variável do controler.
		this.usuarioController = controller;
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.CENTER);
		panel.setLayout(new MigLayout("", "[50px][500px]", "[25px][25px][25px]"));
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(lblNome, "cell 0 0,alignx left,growy");
		
		tfNome = new JTextField();
		panel.add(tfNome, "cell 1 0,grow");
		tfNome.setColumns(50);
		
		JLabel lblUsurio = new JLabel("Usuário");
		panel.add(lblUsurio, "cell 0 1,alignx left,growy");
		
		tfUsuario = new JTextField();
		panel.add(tfUsuario, "cell 1 1,grow");
		tfUsuario.setColumns(25);
		
		JLabel lblNewLabel = new JLabel("Senha");
		panel.add(lblNewLabel, "cell 0 2,alignx left,growy");
		
		tdSenha = new JTextField();
		panel.add(tdSenha, "cell 1 2,grow");
		tdSenha.setColumns(25);
		
		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		add(panel_1, BorderLayout.SOUTH);
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// chama o metodo do controller que gerencia o evento
				usuarioController.salvarUsuario();
			}
		});
		panel_1.add(btnSalvar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//chama o metodo do controller que gerencia o evento
				usuarioController.cancelarForm();
			}
		});
		panel_1.add(btnCancelar);
	}
	
	public UsuarioController getUsuarioController() {
		return usuarioController;
	}

	public void setUsuarioController(UsuarioController usuarioController) {
		this.usuarioController = usuarioController;
	}

	public JTextField getTfNome() {
		return tfNome;
	}

	public void setTfNome(JTextField tfNome) {
		this.tfNome = tfNome;
	}

	public JTextField getTfUsuario() {
		return tfUsuario;
	}

	public void setTfUsuario(JTextField tfUsuario) {
		this.tfUsuario = tfUsuario;
	}

	public JTextField getTfSenha() {
		return tdSenha;
	}

	public void setTfSenha(JTextField tdSenha) {
		this.tdSenha = tdSenha;
	}
	
	
	
}
