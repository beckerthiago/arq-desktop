/*
 * Author: Thiago Becker - beckerthiago@gmail.com
 * MainWindow.java
 */

package com.tbeckr.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import com.tbeckr.gui.controller.UsuarioController;

/**
 * Janela principal da aplicação, contem os menus para acesso a todos
 * os módulos do sistema
 * @author tbecker
 *
 */
public class MainWindow {

	private JFrame frame;

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();		
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame. 
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1013, 727);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnCadastro = new JMenu("Cadastro");
		menuBar.add(mnCadastro);
		
		JMenuItem mntmUsurios = new JMenuItem("Usuários");
		mntmUsurios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Ação de clique do menu usuários
				mntmUsuariosClicked();
			}
		});
		mnCadastro.add(mntmUsurios);
	}
	
	// Ação de clique do menu usuários
	private void mntmUsuariosClicked() {
		//Instância o controller para o módulo de usuários
		UsuarioController uc = new UsuarioController();
		
		// exibe a janela principal do módulo de usuários
		uc.showUsuarioScreen();
	}

}
