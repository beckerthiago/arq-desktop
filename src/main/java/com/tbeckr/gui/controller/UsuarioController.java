/*
 * Author: Thiago Becker - beckerthiago@gmail.com
 * UsuarioController.java
 */

package com.tbeckr.gui.controller;

import java.util.List;

import javax.swing.JTable;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;

import com.tbeckr.business.dal.UsuarioDAO;
import com.tbeckr.business.entity.Usuario;
import com.tbeckr.gui.view.FormUsuarioPanel;
import com.tbeckr.gui.view.ListUsuarioPanel;
import com.tbeckr.gui.view.UsuariosFrame;

/**
 * Classe que controla a interface do módulo de usuários esta classe é
 * responsável por controlar as ações e fluxos das janelas do módulo de
 * usuários, bem como trocar informações da interface com a camada de acesso a
 * dados
 * 
 * Neste exemplo as regras de negócio estão implementadas no próprio controller,
 * porém se houver necessidade pode-se criar uma camada adicionar para
 * implementação de regras de negócio mais complexas.
 * 
 * @author tbecker
 *
 */
public class UsuarioController {

	/*
	 * A interface foi definida da seguinte forma: um JFrame apenas com um
	 * container, neste container são colocados e retirados os panéis que contém
	 * as telas propriamente ditas.
	 */

	// A janela da interface de usuários
	private UsuariosFrame usuariosFrame;

	// O Panel com a interface de listagem
	private ListUsuarioPanel listUsuarioPanel;

	// O Panel com a interface do formulário
	private FormUsuarioPanel formUsuarioPanel;

	private Usuario usuarioSelecionado;
	private UsuarioDAO usuarioDAO = new UsuarioDAO();

	// Constructor padrão
	public UsuarioController() {
		// instancia a janela e os panels

		this.usuariosFrame = new UsuariosFrame(this);
		this.listUsuarioPanel = new ListUsuarioPanel(this);
		this.formUsuarioPanel = new FormUsuarioPanel(this);

		// seta a visibilidade para false para exibir
		// apenas quando o método showUsuarioScreen for chamado
		this.usuariosFrame.setVisible(false);
	}

	// Mostra a tela
	public void showUsuarioScreen() {
		refreshListaUsuarios();
		this.usuariosFrame.pack();
		this.usuariosFrame.setVisible(true);

		// mostra a interface de listagem
		showList();
	}

	/**
	 * Atualiza a lista de usuários a partir do banco de dados
	 */
	public void refreshListaUsuarios() {
		// Limpa a tabela
		((DefaultTableModel) listUsuarioPanel.getTable().getModel()).setRowCount(0);

		// Busca os usuários do banco e popula os dados na tabela
		List<Usuario> usuarios = usuarioDAO.findAll();
		
		for (Usuario usuario : usuarios) {
			((DefaultTableModel) listUsuarioPanel.getTable().getModel())
					.addRow(new Object[] { usuario.getId(), usuario.getNome(), usuario.getLogin() });
		}
	}

	/**
	 * ação do botão novo na interface de listagem
	 */
	public void novoUsuario() {
		/* define o usuário atual com uma nova instância
		 * e abre a interface de cadastro.
		 */
		
		this.usuarioSelecionado = new Usuario();
		showForm();
	}

	/** 
	 * ação do botão cancelar na interface do formulário
	 * volta a visão do formulário para a listagem
	 */
	public void cancelarForm() {
		
		showList();
	}

	/** 
	 * ação do botão fechar na interface da listagem
	 * fecha a janela de usuários e volta para a tela
	 * principal do sistema
	 */
	public void fecharList() {
		
		this.usuariosFrame.dispose();
	}

	/**
	 * ação do botão salvar na interface do formulário
	 */
	public void salvarUsuario() {
		/*
		 * A variável usuário aqui é preenchida com o usuarioSelecionado, se por acaso
		 * o botão salvar for clicado quando um novo usuário está sendo criado a variável
		 * usuarioSelecionado contém um objeto em branco, caso seja uma edição, o objeto
		 * contém o usuário a ser alterado (já com sua chave primaria preenchida)
		 */
		Usuario usuario = this.usuarioSelecionado;

		// pega os valores do fomulário e preenche no objeto usuário
		usuario.setNome(formUsuarioPanel.getTfNome().getText());
		usuario.setLogin(formUsuarioPanel.getTfUsuario().getText());
		usuario.setSenha(formUsuarioPanel.getTfSenha().getText());

		// salva o objeto no banco de dados
		usuarioDAO.save(usuario);

		// atualiza a listagem de usuários
		refreshListaUsuarios();

		// fecha o formulário e volta para a tela de listagem
		showList();
	}

	/**
	 * ação do botão editar na tela de listagem
	 */
	public void editar() {
		/* 
		 * preenche os dados do formulário a partir do usuário selecionado
		 * e mostra a tela do formuário 
		 */
		this.bindForm();		
		showForm();
	}

	/**
	 * ação do botão remover na tela de listagem
	 */
	public void remover() {
		
		// remove o usuário selecionado do banco de dados
		usuarioDAO.delete(usuarioSelecionado);
		
		// instância um usuário vazio para o usuário selecionado
		usuarioSelecionado = new Usuario();
		
		// atualiza a lista de usuários do banco de dados e em seguida atualiza a tela
		refreshListaUsuarios();
		this.usuariosFrame.repaint();
	}

	/**
	 * Atualiza os campos do formulário a partir do usuarioSelecionado
	 */
	public void bindForm() {
		this.formUsuarioPanel.getTfNome().setText(usuarioSelecionado.getNome());		
		this.formUsuarioPanel.getTfUsuario().setText(usuarioSelecionado.getLogin());		
		this.formUsuarioPanel.getTfSenha().setText(usuarioSelecionado.getSenha());		
	}

	/**
	 * Mostra o painel de listagem
	 */
	public void showList() {
		this.usuariosFrame.getContentPane().removeAll();
		this.usuariosFrame.getContentPane().add(listUsuarioPanel);
		this.usuariosFrame.pack();
		this.usuariosFrame.repaint();
	}

	/***
	 * Mostra o painel de formulário
	 */
	public void showForm() {
		this.usuariosFrame.getContentPane().removeAll();
		this.usuariosFrame.getContentPane().add(formUsuarioPanel);
		this.bindForm();
		this.usuariosFrame.pack();
		this.usuariosFrame.repaint();
	}

	/**
	 * Ação realizada ao mudar a seleção do usuário na tela de listagem
	 */
	public void updateUsuarioSelecionado() {
		JTable table = this.listUsuarioPanel.getTable();
		
		// se não há nenhuma linha selecionada, encerra e execução
		if (table.getSelectedRow() == -1)
			return;

		/* pega o valor do Id (que é o primeiro campo da tabela)
		 * foi uma solução simples para manter uma referência a chave primária do usuário na tabela
		 * há formas mais elegantes de fazer isso para não exibir a chave primária na tabela em si,
		 * mas esta funciona.
		 */
		Long id = (long) table.getValueAt(table.getSelectedRow(), 0);

		// busca o usuário no banco a partir da chave primária
		Usuario usuario = usuarioDAO.findById(Usuario.class, id);

		// seta a variável de usuário selecionado com o usuário buscado no banco
		this.usuarioSelecionado = usuario;
	}

	/**
	 * @return o usuário selecionado no momento
	 */
	public Usuario getUsuarioSelecionado() {
		return usuarioSelecionado;
	}

	/**
	 * 
	 * @param usuarioSelecionado o usuário para ser selecionado
	 */
	public void setUsuarioSelecionado(Usuario usuarioSelecionado) {
		this.usuarioSelecionado = usuarioSelecionado;
	}

}
